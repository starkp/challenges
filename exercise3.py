#!/usr/bin/python3
from re import compile, split


def valid_ip(ip_addr):
    valid_ipv4_address = True

    ip_period_pattern = r'\.'
    ip_period_char = compile(ip_period_pattern)

    ipv4_address_pattern = ''

    octet_count = 1

    for octet in split(ip_period_char, ip_addr):
        if len(octet) == 3:
            ipv4_address_pattern = r'^(1[0-9][0-9]|2[0-4][0-9]|2[5][0-5])'
        elif len(octet) == 2:
            ipv4_address_pattern = r'^[1-9][0-9]'
        elif len(octet) == 1 and octet_count == 1:
            ipv4_address_pattern = r'^[1-9]'
        elif len(octet) == 1:
            ipv4_address_pattern = r'^[0-9]'
        else:
            valid_ipv4_address = False

        octet_count += 1
        ipv4_address = compile(ipv4_address_pattern)

        if not ipv4_address.match(octet):
            valid_ipv4_address = False
            break

    return valid_ipv4_address


def rfc_1918(ip_addr_to_eval):
    is_rfc_1918_address = False

    rfc1918_pattern_10_address = r'^10\.'
    rfc1918_10_address = compile(rfc1918_pattern_10_address)

    rfc1918_pattern_172_address = r'^172\.(1[6-9]|2[0-9]|3[0-1])'
    rfc1918_172_address = compile(rfc1918_pattern_172_address)

    rfc1918_pattern_192 = r'^192\.168\.'
    rfc1918_192_address = compile(rfc1918_pattern_192)

    if rfc1918_10_address.match(ip_addr_to_eval):
        is_rfc_1918_address = True

    elif rfc1918_172_address.match(ip_addr_to_eval):
        is_rfc_1918_address = True

    elif rfc1918_192_address.match(ip_addr_to_eval):
        is_rfc_1918_address = True

    return is_rfc_1918_address


if __name__ == '__main__':
    ip_pattern = r'(\d+)\.(\d+)\.(\d+)\.(\d+)'
    potential_ip = compile(ip_pattern)

    whitespace_pattern = r'\s'
    whitespace_char = compile(whitespace_pattern)

    filename = 'exercise3.txt'

    print('Processing contents of {}'.format(filename))
    print()

    print('Valid IP Address(es) Found')
    print('--------------------------')

    with open(filename) as file_to_process:
        for cnt, line in enumerate(file_to_process):
            for word in split(whitespace_char, line):
                if potential_ip.match(word) and valid_ip(word) and not rfc_1918(word):
                    print("{}".format(word))
