#!/usr/bin/python3
from pyghappy import PyGhappy
from pprint import pprint
from base64 import b64decode

api_token = ''
repo_owner = 'octocat'
repo_name = 'Hello-World'
sha_hash_of_commit = '762941318ee16e59dabbacb1b4049eec22f0d303'
path = 'README'

personal_repo_owner = ''
personal_repo_name = 'test'
personal_repo_path = 'test.txt'

file_contents = ''

with open('test.txt', 'w') as file_to_write:
    file_to_write.write('Hello World!')

with open('test.txt', 'rb') as file_to_test:
    for cnt, line in enumerate(file_to_test):
        file_contents = file_contents.encode() + line

git = PyGhappy(api_token)

# List the commits in a git repo
print('Getting the list of commits from the {} repo owned by {}'.format(repo_name, repo_owner))
print()
list_of_repo_commits = git.get_repo_commits(repo_owner, repo_name)
pprint(list_of_repo_commits)

print('\n-----------------------------------------------------------------------------------------')
# Get a single commit from a git repo based on the hash of the commit
print('Getting the {} commit from the {} repo owned by {}'.format(sha_hash_of_commit, repo_name, repo_owner))
print()
single_git_commit = git.get_single_repo_commit(repo_owner, repo_name, sha_hash=sha_hash_of_commit)
pprint(single_git_commit)

print('\n-----------------------------------------------------------------------------------------')
# List the contents of a file in a git repo
print('Retrieving the contents of {} from the {} repo owned by {}'.format(path, repo_name, repo_owner))
print()

get_file_contents = git.get_repo_contents(repo_owner, repo_name, path=path)
pprint(get_file_contents)

print()
print('The base64 decoded contents of the {} file are:'.format(path))
print('-----------------------------------------------------')
print(b64decode(get_file_contents['content']))

print('\n-----------------------------------------------------------------------------------------')
# Create a file in a git repo
print('Committing {} to the {} repo owned by {}'.format(personal_repo_path, personal_repo_name, personal_repo_owner))
print()

create_file_git = git.create_repo_content(personal_repo_owner, personal_repo_name, path=personal_repo_path,
                                          message='Test Commit', content=file_contents)

pprint(create_file_git)

print('\n-----------------------------------------------------------------------------------------')
# Modify an existing file in a git repo
with open('test.txt', 'w') as file_to_write:
    file_to_write.write('Hello World! - Modified')

file_contents = ''

with open('test.txt', 'rb') as file_to_test:
    for cnt, line in enumerate(file_to_test):
        file_contents = file_contents.encode() + line

sha_hash_of_file_to_replace = create_file_git['content']['sha']

print('Updating {} in the {} repo owned by {} with the hash of {}'.format(personal_repo_path,
                                                                          personal_repo_name,
                                                                          personal_repo_owner,
                                                                          sha_hash_of_file_to_replace
                                                                          ))
print()

update_file_git = git.update_repo_content(personal_repo_owner,
                                          personal_repo_name,
                                          path=personal_repo_path,
                                          message='Updated contents of file',
                                          content=file_contents,
                                          sha_hash=sha_hash_of_file_to_replace
                                          )
pprint(update_file_git)

print('\n-----------------------------------------------------------------------------------------')
# Modify an existing file in a git repo
sha_hash_of_file_to_delete = update_file_git['content']['sha']
print('Deleting {} from the {} repo owned by {} with the hash of {}'.format(personal_repo_path,
                                                                            personal_repo_name,
                                                                            personal_repo_owner,
                                                                            sha_hash_of_file_to_delete))
print()

delete_file_git = git.delete_repo_content(personal_repo_owner,
                                          personal_repo_name,
                                          path=personal_repo_path,
                                          message='Remove Test Commit',
                                          sha_hash=sha_hash_of_file_to_delete
                                          )
pprint(delete_file_git)
