from requests import get, put, delete, codes
from binascii import b2a_base64

class PyGhappy:
    def __init__(self, authtoken, baseurl='https://api.github.com', api_version='application/vnd.github.v3+json'):
        self.authtoken = authtoken
        self.baseurl = baseurl
        self.api_version = api_version

    def base64_encode_content(self, content_to_encode):
        content_to_encode = b2a_base64(content_to_encode)
        content_to_encode = content_to_encode.decode()

        return content_to_encode

    def create_http_headers(self):
        http_headers = {'Accept': self.api_version, 'Authorization': 'token ' + self.authtoken}

        return http_headers

    def create_http_request(self, url_to_submit, http_verb_type='GET', list_of_parameters=None):
        if http_verb_type == 'GET':
            construct_http_request = get(url_to_submit, params=list_of_parameters, headers=self.create_http_headers())
        elif http_verb_type == 'PUT':
            construct_http_request = put(url_to_submit, json=list_of_parameters, headers=self.create_http_headers())
        elif http_verb_type == 'DELETE':
            construct_http_request = delete(url_to_submit, json=list_of_parameters, headers=self.create_http_headers())

        if construct_http_request.status_code == codes.ok:
            return construct_http_request.json()
        elif construct_http_request.status_code == 201:
            return construct_http_request.json()
        else:
            return construct_http_request.json()['message']

    def get_repo_contents(self, owner, repo, path, ref=None):
        self.repo_contents_url = self.baseurl + '/repos/' + owner + '/' + repo + '/contents/' + path

        parameters_to_include = {'ref': ref}
        contents = self.create_http_request(self.repo_contents_url, list_of_parameters=parameters_to_include)

        return contents

    def create_repo_content(self, owner, repo, path, message, content, branch=None):
        self.repo_contents_url = self.baseurl + '/repos/' + owner + '/' + repo + '/contents/' + path

        if content is not None:
            content = self.base64_encode_content(content)

        if branch is not None:
            parameters_to_include = {'message': message, 'content': content, 'branch': branch}
        else:
            parameters_to_include = {'message': message, 'content': content}

        create_content = self.create_http_request(self.repo_contents_url,
                                                  http_verb_type='PUT',
                                                  list_of_parameters=parameters_to_include)

        return create_content

    def update_repo_content(self, owner, repo, path, message, content, sha_hash, branch=None):
        self.repo_contents_url = self.baseurl + '/repos/' + owner + '/' + repo + '/contents/' + path

        if content is not None:
            content = self.base64_encode_content(content)

        if branch is not None:
            parameters_to_include = {'message': message, 'content': content, 'sha': sha_hash, 'branch': branch}
        else:
            parameters_to_include = {'message': message, 'content': content, 'sha': sha_hash}

        update_content = self.create_http_request(self.repo_contents_url,
                                                  http_verb_type='PUT',
                                                  list_of_parameters=parameters_to_include)

        return update_content

    def delete_repo_content(self, owner, repo, path, message, sha_hash, branch=None):
        self.repo_contents_url = self.baseurl + '/repos/' + owner + '/' + repo + '/contents/' + path

        if branch is not None:
            parameters_to_include = {'message': message, 'sha': sha_hash, 'branch': branch}
        else:
            parameters_to_include = {'message': message, 'sha': sha_hash}

        delete_content = self.create_http_request(self.repo_contents_url,
                                                  http_verb_type='DELETE',
                                                  list_of_parameters=parameters_to_include)

        return delete_content

    def get_repo_commits(self, owner, repo, sha_or_branch=None, path=None, author=None, since=None, until=None):
        self.repo_commits_url = self.baseurl + '/repos/' + owner + '/' + repo + '/commits'

        parameters_to_include = {'sha_or_branch': sha_or_branch, 'path': path, 'author': author, 'since': since,
                                 'until': until}

        commits = self.create_http_request(self.repo_commits_url, list_of_parameters=parameters_to_include)

        return commits

    def get_single_repo_commit(self, owner, repo, sha_hash):
        self.repo_single_commit_url = self.baseurl + '/repos/' + owner + '/' + repo + '/commits/' + sha_hash

        git_single_commit = self.create_http_request(self.repo_single_commit_url)

        return git_single_commit
