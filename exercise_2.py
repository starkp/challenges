#!/usr/bin/python3
from multiprocessing import Pool
from json import load
from csv import DictWriter, QUOTE_NONE
from collections import OrderedDict


def process_file(file_to_update):
    file_to_update = list(file_to_update)

    for cnt, line in enumerate(file_to_update):
        for column in line:
            new_value = line[column] * len(column)
            line[column] = new_value

    return file_to_update


def extract_column_headers(file_to_extract):
    column_names = OrderedDict()

    for cnt, line in enumerate(file_to_extract):
        for column in line:
            column_names[column] = cnt + 1

    column_header = [column_name for column_name in column_names.keys()]

    return column_header


def use_process_pool(process_pool_name, function_to_use, function_argument):
    data_returned_from_function = process_pool_name.map(function_to_use, (function_argument,))

    for data_returned in data_returned_from_function:
        data_to_return = data_returned

    return data_to_return


def write_csv_file(output_file_name, column_headers_to_use, data_to_write, dialect_to_use='unix', quoting_to_use=QUOTE_NONE):
    with open(output_file_name, 'w') as csv_file:
        writer = DictWriter(csv_file, fieldnames=column_headers_to_use, dialect=dialect_to_use, quoting=quoting_to_use)

        writer.writeheader()

        for data in data_to_write:
            writer.writerow(data)


if __name__ == '__main__':
    number_of_processes = 3
    input_filename = 'exercise2.json'
    output_filename = 'exercise2.csv'

    print('Reading contents of {}'.format(input_filename))

    with Pool(processes=number_of_processes) as pool:
        with open(input_filename) as file_to_process:
            print('Processing contents of {}'.format(input_filename))
            json_file = load(file_to_process)

            retrieve_column_headers = use_process_pool(pool, extract_column_headers, json_file)
            values_to_process = use_process_pool(pool, process_file, json_file)

            print('Writing results to {}'.format(output_filename))
            write_csv_file(output_filename, retrieve_column_headers, values_to_process)
